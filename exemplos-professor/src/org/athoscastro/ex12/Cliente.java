package org.athoscastro.ex12;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import javax.swing.JOptionPane;

public class Cliente {

    public static void main(String[] args) {
        try {
            Registry registro = LocateRegistry.getRegistry("Localhost", 1099);
            Analisadora canal = (Analisadora) registro.lookup("metodosAnalisadores");

            String valor = JOptionPane.showInputDialog("Digite um valor inteiro");
            if (canal.maiorMenorZero(Integer.parseInt(valor))) {
                JOptionPane.showMessageDialog(null, "Valor digitado maior que zero");
            }else JOptionPane.showMessageDialog(null, "Valor digitado menor ou igual a zero");
            
            String digito = JOptionPane.showInputDialog("Digite uma sequência de números ou letras");
            JOptionPane.showMessageDialog(null, "Total de caracteres = "+canal.verficaDigito(digito));
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
