package org.athoscastro.ex03.produtor;

public interface Buffer {
    
    public void set(int valor);
    
    public int get();
    
}
