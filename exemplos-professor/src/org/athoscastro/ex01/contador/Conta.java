
package org.athoscastro.ex01.contador;


public interface Conta {
    public void incC();
    public void decC();
    public int mostraC();    
}
