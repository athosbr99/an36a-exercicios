package org.athoscastro.ex1;

class NovaThread implements Runnable {
    Thread t;

    NovaThread() {
        t = new Thread(this, "Thread Demo");
        System.out.println("Thread filha: " + t);
    }

    public void run() {
        try {
            for (int i = 5; i > 0; i--) {
                System.out.println("Thread filha: " + i);
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            System.out.printf("Thread interrompida");
        }
        System.out.println("Saindo da thread filha.");
    }
}

class NovaThread2 extends Thread {
    NovaThread2() {
        super("Demo Thread");
        System.out.println("Thread filha: " + this);
    }

    public void run() {
        try {
            for(int i = 5; i > 0; i--) {
                System.out.println("Thread filha: " + i);
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            System.out.println("Thread interrompida.");
        }
        System.out.println("Saindo da thread filha.");
    }
}

public class Main {

    public static void main(String[] args) {
	    Thread t = Thread.currentThread();
        System.out.println("Thread atual: " + t);
        t.setName("Batata");
        System.out.println("Após mudança: " + t);

        try{
            for(int i = 5; i > 0; i--) {
                System.out.println(i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println("Thread interrompida.");
        }

        NovaThread nt = new NovaThread();
        System.out.println(nt.t.getName());
        nt.t.start();
        try {
            for(int i = 5; i > 0; i--) {
                System.out.println("Thread principal: " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println("Thread interrompida.");
        }
    }
}
