package org.athoscastro.ex4;

public class Producer implements Runnable {
    Queue q;
    Thread t;

    Producer(Queue q) {
        this.q = q;
        t = new Thread(this, "Producer");
    }

    public void run() {
        int i = 0;
        while (i < 50) {
            q.put(i++);
        }
    }
}
