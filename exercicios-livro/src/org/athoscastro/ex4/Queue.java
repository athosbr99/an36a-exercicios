package org.athoscastro.ex4;

public class Queue {
    int n;
    boolean valueSet = false;

    synchronized int get() {
        while (!valueSet) {
            try {
                wait();
            } catch(InterruptedException e) {
                System.out.println("Interrompido.");
            }
        }
        System.out.println("Tenho: " + n);
        valueSet = false;
        notify();
        return n;
    }

    synchronized void put(int n) {
        while(valueSet) {
            try {
                wait();
            } catch(InterruptedException e) {
                System.out.println("Interrompido.");
            }
        }
        this.n = n;
        valueSet = true;
        notify();
        System.out.println("Coloquei: " + n);
    }
}
