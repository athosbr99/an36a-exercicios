package org.athoscastro.ex2;

class ThreadJoin implements Runnable {
    String nome;
    Thread t;

    ThreadJoin(String nome) {
        this.nome = nome;
        this.t = new Thread(this, this.nome);
        System.out.println("Nova thread: " + this.t);
    }

    public void run() {
        try {
            for(int i = 5; i > 0; i--) {
                System.out.println(nome + ": " + i);
                Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            System.out.println(nome + " interrompido");
        }
        System.out.println("Saindo de " + nome);
    }
}

public class Main {
    public static void main(String args[]) {
        ThreadJoin t1 = new ThreadJoin("Thread um");
        ThreadJoin t2 = new ThreadJoin("Thread dois");
        ThreadJoin t3 = new ThreadJoin("Thread tres");
        t1.t.start();
        t2.t.start();
        t3.t.start();
        System.out.println("Thread um está viva: " + t1.t.isAlive());
        System.out.println("Thread dois está viva: " + t2.t.isAlive());
        System.out.println("Thread três está viva: " + t3.t.isAlive());
        try {
            t1.t.join();
            t2.t.join();
            t3.t.join();
        } catch (InterruptedException e) {
            System.out.println("Thread interrompida");
        }
        System.out.println("Thread um está viva: " + t1.t.isAlive());
        System.out.println("Thread dois está viva: " + t2.t.isAlive());
        System.out.println("Thread três está viva: " + t3.t.isAlive());
    }
}
