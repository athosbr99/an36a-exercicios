package org.athoscastro.ex5;

class A {
    synchronized void foo(B b) {
        System.out.println(Thread.currentThread().getName() + " entrou em A.foo");
        try {
            Thread.sleep(1000);
        } catch(InterruptedException e) {
            System.out.println("Interrompido");
        }
        System.out.println(Thread.currentThread().getName() + " tentando chamar B.last");
        b.last();
    }

    synchronized void last() {
        System.out.println("Dentro de A.last");
    }
}

class B {
   synchronized void bar(A a) {
       System.out.println(Thread.currentThread().getName() + " entrou em B.foo");
       try {
           Thread.sleep(1000);
       } catch(InterruptedException e) {
           System.out.println("Interrompido");
       }
       System.out.println(Thread.currentThread().getName() + " tentando chamar A.last");
       a.last();
   }

   synchronized void last() {
       System.out.println("Dentro de B.last");
   }
}

public class Deadlock implements Runnable {
    A a = new A();
    B b = new B();
    Thread t;

    Deadlock() {
        Thread.currentThread().setName("MainThread");
        t = new Thread(this, "RacingThread");
    }

    void deadlockStart() {
        t.start();
        a.foo(b);
        System.out.println("De volta para a thread principal.");
    }

    public void run() {
        b.bar(a);
        System.out.printf("De volta a outra thread.");
    }

    public static void main(String[] args) {
        Deadlock dl = new Deadlock();
        dl.deadlockStart();
    }

}
